/**
 * @file mymenu.h
 * @brief The declaration of class MyMenu.
 * @version 4.0
 * @date 2022-12-09
 */
#ifndef MY_MENU_H
#define MY_MENU_H

#include "global.h"

#include <QList>
#include <QMap>
#include <QMenuBar>
#include <QAction>
#include <QPushButton>
#include <QMenu>

/**
 * @brief  Class MyMenu is the menu bar part for QMainWindow.
 * MyMenu includes info of this project, file open/close,
 * and language switch.
 */
class MyMenu : public QMenuBar
{
public:
    /**
     * @brief Construct a new MyMenu object
     * 
     * Initialize all the data members, bind signal and slots.
     * 
     * @param parent        The parent object of class object.
     * @param default_lan   The default language of menubar.
     */
    MyMenu(QWidget* parent, LANGUAGE default_lan);

    /**
     * @brief Destroy the MyMenu object.
     * 
     */
    ~MyMenu() = default;

public:
    /**
     * @brief The default language of menu.
     * 
     * As designed, the default language is English.
     * 
     */
    LANGUAGE                    _default_lan;
    /**
     * @brief The QAction object pointer.
     * 
     * If this action is triggered, 
     * a slot function `showInfo` will be called, 
     * which will show the info of this project.
     * 
     */
    QAction*                    _info;
    /**
     * @brief The QMap object pointer.
     * 
     * The map stores `QAction` object pointers,
     * which will be shown when the sub-menu `_menu_file` 
     * is clicked.
     * 
     * The pointers stored in this `QMap` object point 
     * `QAction` objects that :
     *      1. open a video file
     *      2. open a folder, load the video files inside it
     *      3. close curretn video file
     * 
     */
    QMap<QString, QAction*>     _map_file;
    /**
     * @brief The QMap object pointer.
     * 
     * The map stores 'QAction' object pointers,
     * which will be shown when the sub-menu `_menu_language`
     * is clicked.
     * 
     * The pointers stored in this `QMap` object point 
     * `QAction` objects that switch to different languages.
     * Languages currently supported are:
     *      1. English(default)
     *      2. Simplified Chinese
     * 
     */
    QMap<QString, QAction*>     _map_language;
    /**
     * @brief The `QMenu` object, storing `QAction` objects of
     * file operation.
     * 
     */
    QMenu*                      _menu_file;
    /**
     * @brief The `QMenu` object, storing `QAction` objects of
     * language switch.
     * 
     */
    QMenu*                      _menu_language;
    
    /**
     * @brief Set the Language of MenuBar.
     * 
     * @param lan The enum item, specifying the language to switch.
     */
    void setLan(LANGUAGE lan);
    
private slots:
    /**
     * @brief Show information about this project.
     * 
     * A `QMessageBox` instance will be created. 
     * It shows:
     *      1. The version of this project
     *      2. The members of this project
     *      3. Other additional info
     * 
     */
    void showInfo();
};

#endif // MYMENU_H

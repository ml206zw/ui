/**
 * @file main.cpp
 * @brief The entrence of the app.
 * @version 4.0
 * @date 2022-12-09
 */
#include "mycontrol.h"
#include "myplayer.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    MyPlayer mainWindow;
    mainWindow.show();
    
    return app.exec();
}

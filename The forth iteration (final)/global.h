/**
 * @file global.h
 * @brief The global configuration of project.
 * @version 2.0
 * @date 2022-12-09
 */
#ifndef GLOBAL_H
#define GLOBAL_H

/**
 * \enum LANGUAGE
 * This enumeration represents the different languages that the program supports.
 * \var ENGLISH
 * Identifies the English language.
 * \var CHINESE
 * Identifies the Chinese language.
 */
enum LANGUAGE
{
    ENGLISH,
    CHINESE
};

/**
 * @brief Debug macro function. Used to print log when a slot 
 * function is triggered.
 * 
 */
#define DEBUG_CALL_FUNC()   qDebug() << "Call " << __func__;

// #define VIDEO_TYPES "*.mp4 *.avi *.ps *.ts *.asf *.flv *.dahv *.mov"
#define VIDEO_TYPES "*.wmv"

// #define DEFAULT_FILE_DIRECTORY  "C:"
#define DEFAULT_FILE_DIRECTORY  "E:\\Desktop\\Not_finished\\2022-11-27 tec qt\\2811_cw2-master-release\\videos"

#endif // GLOBAL_H

/**
 * @file mycontrol.h
 * @brief The declaration of class MyControl.
 * @version 4.0
 * @date 2022-12-09
 */
#ifndef MYCONTROL_H
#define MYCONTROL_H

#include "global.h"

#include <QLabel>
#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QSlider>
#include <QPushButton>
#include <QSpacerItem>

/**
 * @brief The main control panel for MyControl.
 * 
 * This class is responsible for creating the control panel for MyPlayer, which
 * consists of a title label, time label, time slider, control buttons, spacer,
 * and volume slider.
 */
class MyControl : public QWidget
{
    Q_OBJECT
public:
    /**
     * @brief Constructs a MyControl object.
     * @param parent The parent widget.
     */
    explicit MyControl(QWidget *parent = nullptr);
    
public:
    /**
     * @brief The default language of MyControl.
     */
    LANGUAGE        _default_lan;
    /**
     * @brief The main layout of the control panel.
     */
    QVBoxLayout*    _main_layout;
    /**
     * @brief The title label of the control panel.
     */
    QLabel*         _title;
    /**
     * @brief The time layout of the control panel.
     */
    QHBoxLayout*    _time_layout;
    /**
     * @brief The media time of the control panel.
     */
    QLabel*         _media_time;
    /**
     * @brief The time slider of the control panel.
     */
    QSlider*        _time_slider;
    /**
     * @brief The control layout of the control panel.
     */
    QHBoxLayout*    _control_layout;
    /**
     * @brief The previous video pushbutton of the control panel.
     */
    QPushButton*    _pb_prev;
    /**
     * @brief The pause video pushbutton of the control panel.
     */
    QPushButton*    _pb_pause;
    /**
     * @brief The next video pushbutton of the control panel.
     */
    QPushButton*    _pb_next;
    /**
     * @brief The spacer to format layout of the control panel.
     */
    QSpacerItem*    _spacer;
    /**
     * @brief The volume slider of the control panel.
     */
    QSlider*        _volume_slider;
    
    /**
     * @brief Return the string for no video playing in language `_default_lan`
     */
    QString getStr_notSelect();
    
    /**
     * @brief Return the string for video playing in language `_default_lan`
     */
    QString getStr_playing();
    
public slots:
};

#endif // MYCONTROL_H

/**
 * @file mymenu.cpp
 * @brief The definition of member functions 
 *        of class MyMenu
 * @version 4.0
 * @date 2022-12-09
 */
#include "mymenu.h"

#include <QDebug>
#include <QList>
#include <QAction>
#include <QMenu>
#include <QPushButton>
#include <QMessageBox>

using namespace std;

MyMenu::MyMenu(QWidget* parent,
               LANGUAGE default_lan) :
    QMenuBar(parent),
    _default_lan(default_lan),
    _info(new QAction("info", this)),
    _map_file(),
    _map_language(),
    _menu_file(new QMenu("file", this)),
    _menu_language(new QMenu("language", this))
{
    // add choices into map
    _map_file["open video"]     = new QAction("open video", this);
    _map_file["open folder"]    = new QAction("open folder", this);
    _map_file["close all"]      = new QAction("close all", this);
    
    // add languages into map
    _map_language["English"] = new QAction("English(default)", this);
    _map_language["Chinese"] = new QAction("中文", this);
    
    // add choices into "file" action
    this->_menu_file->addAction(_map_file["open video"]);
    this->_menu_file->addAction(_map_file["open folder"]);
    // add a seperating line
    this->_menu_file->addSeparator();
    this->_menu_file->addAction(_map_file["close all"]);
    
    // add choices into "language" action
    this->_menu_language->addAction(_map_language["English"]);
    this->_menu_language->addAction( _map_language["Chinese"]);
    
    // add actions into menu
    this->addAction(this->_info);
    this->addMenu(this->_menu_file);
    this->addMenu(this->_menu_language);
    
    // when "info" is clicked, showInfo function is triggered
    connect(_info, QAction::triggered, this, MyMenu::showInfo);
}

void MyMenu::showInfo()
{
    DEBUG_CALL_FUNC();
    
    switch (_default_lan) 
    {
    default:
    case ENGLISH:
        QMessageBox::about(this, "About This", 
                           "This is a vedio player project of version 4.0");
        break;
    case CHINESE:
        QMessageBox::about(this, "关于我们", 
                           "这是一个视频播放器，版本号：4.0");
        break;
    }
    
}

void MyMenu::setLan(LANGUAGE lan)
{
    this->_default_lan = lan;
    
    switch (lan) {
    default:
    case ENGLISH:
        _info->setText("info");
        
        _map_file["open video"] ->setText("open video");
        _map_file["open folder"]->setText("open folder");
        _map_file["close all"]  ->setText("close all");
        
        _menu_file->setTitle("file");
        _menu_language->setTitle("language");
        break;
    case CHINESE:
        _info->setText("信息");
        
        _map_file["open video"] ->setText("打开视频");
        _map_file["open folder"]->setText("打开文件夹");
        _map_file["close all"]  ->setText("全部关闭");
        
        _menu_file->setTitle("文件");
        _menu_language->setTitle("语言");
        break;
    }
}

#include "mycontrol.h"

MyControl::MyControl(QWidget *parent) : 
    QWidget(parent),
    _default_lan    (ENGLISH),
    _main_layout    (new QVBoxLayout(this)),
    _title          (new QLabel(this)),
    _time_layout    (new QHBoxLayout(this)),
    _media_time     (new QLabel(this)),   
    _time_slider    (new QSlider(Qt::Horizontal, this)),
    _control_layout (new QHBoxLayout(this)),
    _pb_prev        (new QPushButton(this)),  
    _pb_pause       (new QPushButton(this)),     
    _pb_next        (new QPushButton(this)),     
    _spacer         (new QSpacerItem(40, 20, 
                                     QSizePolicy::Expanding, 
                                     QSizePolicy::Minimum)),      
    _volume_slider  (new QSlider(Qt::Horizontal, this))
{
    // set the layout of window
    this->setLayout(_main_layout);
    
    // add title
    _main_layout->addWidget(_title);
    // add time layout, which includes slider and time label
    _main_layout->addLayout(_time_layout);
    // add control layout, which includes previous/pause/next button and
    // volume label
    _main_layout->addLayout(_control_layout);
    
    // add time slider into time layout
    _time_layout->addWidget(_time_slider);
    // add time label into time layout
    _time_layout->addWidget(_media_time);

    // add previous pushbutton into layout
    _control_layout->addWidget(_pb_prev); 
    // add pause pushbutton into layout
    _control_layout->addWidget(_pb_pause); 
    // add next pushbutton into layout
    _control_layout->addWidget(_pb_next);
    // add spacer into layout
    _control_layout->addItem(_spacer);
    // add volume slider into layout
    _control_layout->addWidget(_volume_slider);
    
    // set title text
    _title->setText(getStr_notSelect());
    // set the initia time of time slider
    _time_slider->setValue(0);
    // set the minimum time of time slider
    _time_slider->setMinimum(0);
    // set the maximum time of time slider
    _time_slider->setMaximum(100);
    // set the initia time of time label
    _media_time->setText("00:00:00");
    
    // _pb_prev->setText("<");
    // before a video is loaded, the button is not active
    _pb_prev->setEnabled(false);
    // load icon
    _pb_prev->setIcon(QIcon(":/control_bar_icons/icons/prev.png"));
    // set size
    _pb_prev->setFixedSize(_pb_prev->width(), _pb_prev->height());
    // _pb_pause->setText("=");
    _pb_pause->setEnabled(false);
    _pb_pause->setIcon(QIcon(":/control_bar_icons/icons/pause.png"));
    _pb_pause->setFixedSize(_pb_prev->width(), _pb_prev->height());
    // _pb_next->setText(">");
    _pb_next->setEnabled(false);
    _pb_next->setIcon(QIcon(":/control_bar_icons/icons/next.png"));
    _pb_next->setFixedSize(_pb_prev->width(), _pb_prev->height());
    
    // set initia volume value
    _volume_slider->setValue(0);
    // set minimum value
    _volume_slider->setMinimum(0);
    // set maximum value
    _volume_slider->setMaximum(100);
}

QString MyControl::getStr_notSelect()
{
    switch (_default_lan) 
    {
    default:
    case ENGLISH:
        return "No Video Playing...";
    case CHINESE:
        return "未选择文件";
    }
}

QString MyControl::getStr_playing()
{
    switch (_default_lan) 
    {
    default:
    case ENGLISH:
        return "Playing ";
    case CHINESE:
        return "正在播放 ";
    }
}

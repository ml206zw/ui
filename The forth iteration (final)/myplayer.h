/**
 * @file myplayer.h
 * @brief The head file of main window
 * @version 4.0
 * @date 2022-12-09
 */
#ifndef MYPLAYER_H
#define MYPLAYER_H

#include "global.h"
#include "mymenu.h"
#include "mycontrol.h"

#include <QMainWindow>
#include <QFileDialog>
#include <QMediaPlayer>
#include <QMediaMetaData>
#include <QListWidget>
#include <QTime>
#include <QWidget>
#include <QSize>
#include <QMap>

/** The minimum size of the window. */
constexpr QSize SIZE_MINUMUM_WINDOW = QSize(800, 680);
/** The maximum size of the window. */
constexpr QSize SIZE_MAXIMUM_WINDOW = QSize(1920, 1080);
/** The fixed width of the playlist. */
constexpr int   SIZE_FIXED_LIST_WIDTH = 400;
/** The fixed height of the control bar. */
constexpr int   SIZE_FIXED_CONTROL_HEIGHT = 200;

/**
 * @class MyPlayer
 * @brief This class represents a main window of the media player, including the menu, playlist,
 *          control bar, and video widget.
 * @details This class is a subclass of QMainWindow and provides a player interface with a menu, video widget, play list, and control bar.
 * The class stores the default language and a QFileInfo object to keep track of the currently opened file. It also has a QMediaPlayer and QVideoWidget to play videos.
 * The play list is implemented using a QListWidget and the control bar is a MyControl object.
 * The class uses QMap to store the video files, where the key is a video name of type QString and the value is a video of type QUrl.
 */
class MyPlayer : public QMainWindow
{
    Q_OBJECT
    
public:
    /**
     * @brief Construct a new MyPlayer object
     *
     * @param parent The parent widget of this window.
     */
    MyPlayer(QWidget *parent = nullptr);
    /**
     * @brief Destroy the MyPlayer object
     */
    ~MyPlayer();
    
    /**
     * @brief Init the window, including :
     * 
     * 1. set the minimum & maximum size of window
     * 2. set the title of window
     */
    void init();
    
private:    
    /**
     * @brief The default language for the player interface.
     */
    LANGUAGE                    _default_lan;
    /**
     * @brief Pointer to the menu object.
     */
    MyMenu*                     _menu;
    /**
     * @brief Stores information about the currently opened file.
     */
    QFileInfo                   _fileInfo;
    /**
     * @brief Pointer to the QMediaPlayer object to play videos.
     */
    QMediaPlayer*               _player;
    /**
     * @brief Pointer to the QVideoWidget where the video is displayed.
     */
    QVideoWidget*               _video_widget;
    /**
     * @brief Pointer to the QListWidget for the play list.
     */
    QListWidget*                _play_list;
    /**
     * @brief Pointer to the MyControl object for the control bar.
     */
    MyControl*                  _control_bar;
    /**
     * @brief Typedef for the video name used in the QMap of video files.
     */
    typedef QString video_name;
    /**
     * @brief Typedef for the video URL used in the QMap of video files.
     */
    typedef QUrl    video;
    /**
     * @brief QMap to store the video files where the key is the video name and the value is the video URL.
     */
    QMap<video_name, video>  _video_files; 
    
    /**
     * @brief Sets the text for the file title based on the specified language.
     * @param lan The specified language.
     * @return The text for the file title in the specified language.
     */
    inline QString setText_getFile_title(LANGUAGE lan);
    
    /**
     * @brief Sets the text for the file type based on the specified language.
     * @param lan The specified language.
     * @return The text for the file type in the specified language.
     */
    inline QString setText_getFile_type(LANGUAGE lan);
    
    /**
     * @brief Sets the text for the directory selecting window title based on 
     *      the specified language.
     * @param lan The specified language.
     * @return The text for the directory title in the specified language.
     */
    inline QString setText_getDir_title(LANGUAGE lan);
    
    /**
     * @brief Sets the language for the window and its components.
     * @param lan The specified language.
     */
    void setLan(LANGUAGE lan);
    
    /**
     * @brief Updates the language of the control bar.
     */
    void updateControlLan();
    
private slots:
    /**
     * @brief Handles the resizing event of the window.
     * @param event The resize event.
     */
    void resizeEvent(QResizeEvent* event);
    
    /**
     * @brief Opens a video file.
     */
    void openVideo();
    
    /**
     * @brief Opens a directory and adds all the video files in it to the playlist.
     */
    void openFolder();
    
    /**
     * @brief Closes the current video and clear playlist.
     */
    void closeVideo();
    
    /**
     * @brief Sets the language to English.
     */
    void setLanguage_en();
    
    /**
     * @brief Sets the language to Chinese.
     */
    void setLanguage_cn();
    
    /**
     * @brief Handles the event of changing the play state of the video.
     * @param ms The new play state.
     */
    void playStateChanged (QMediaPlayer::State ms);
    
    /**
     * @brief Judges whether the video has reached the end and performs necessary actions.
     * @param position The current position of the video.
     */
    void judgeVideoTime(qint64 position);
    
    /**
     * @brief Sets the current video process based on the specified position.
     * @param position The specified position.
     */
    void setVideoProcess(qint64 position);
    
    /**
     * @brief Judges whether the volume of the video should be changed and performs necessary actions.
     */
    void judgeVolume();
    
    /**
     * @brief Changes the play state of the video, if current video is playing, pauses it. 
     *      If current video is paused, plays it.
     */
    void changePlayerStatus();
    
    /**
     * @brief Plays the next video in the playlist, if the playlist is empty, nothing happens
     */
    void nextVideo();
    
    /**
     * @brief Plays the previous video in the playlist, if the playlist is empty, nothing happens
     */
    void prevVideo();
    
    /**
     * @brief Handles the event of clicking on a video in the playlist.
     * @param item The clicked video item.
     */
    void videoClicked(QListWidgetItem *item);
};
#endif // MYPLAYER_H

#ifndef MYCONTROL_H
#define MYCONTROL_H

#include "global.h"

#include <QLabel>
#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QSlider>
#include <QPushButton>
#include <QSpacerItem>

class MyControl : public QWidget
{
    Q_OBJECT
public:
    explicit MyControl(QWidget *parent = nullptr);
    
public:
    LANGUAGE        _default_lan;
    QVBoxLayout*    _main_layout;
    QLabel*         _title;
    QHBoxLayout*    _time_layout;
    QLabel*         _media_time;
    QSlider*        _time_slider;
    QHBoxLayout*    _control_layout;
    QPushButton*    _pb_prev;
    QPushButton*    _pb_pause;
    QPushButton*    _pb_next;
    QSpacerItem*    _spacer;
    QSlider*        _volume_slider;
    
    QString getStr_notSelect();
    
    QString getStr_playing();
    
public slots:
};

#endif // MYCONTROL_H

#include "mycontrol.h"

MyControl::MyControl(QWidget *parent) : 
    QWidget(parent),
    _default_lan    (ENGLISH),
    _main_layout    (new QVBoxLayout(this)),
    _title          (new QLabel(this)),
    _time_layout    (new QHBoxLayout(this)),
    _media_time     (new QLabel(this)),   
    _time_slider    (new QSlider(Qt::Horizontal, this)),
    _control_layout (new QHBoxLayout(this)),
    _pb_prev        (new QPushButton(this)),  
    _pb_pause       (new QPushButton(this)),     
    _pb_next        (new QPushButton(this)),     
    _spacer         (new QSpacerItem(40, 20, 
                                     QSizePolicy::Expanding, 
                                     QSizePolicy::Minimum)),      
    _volume_slider  (new QSlider(Qt::Horizontal, this))
{
    this->setLayout(_main_layout);
    
    _main_layout->addWidget(_title);
    _main_layout->addLayout(_time_layout);
    _main_layout->addLayout(_control_layout);
    
    _time_layout->addWidget(_time_slider);
    _time_layout->addWidget(_media_time);

    _control_layout->addWidget(_pb_prev); 
    _control_layout->addWidget(_pb_pause); 
    _control_layout->addWidget(_pb_next);
    _control_layout->addItem(_spacer);
    _control_layout->addWidget(_volume_slider);
    
    _title->setText(getStr_notSelect());
    _time_slider->setValue(0);
    _time_slider->setMinimum(0);
    _time_slider->setMaximum(100);
    _media_time->setText("00:00:00");
    
    _pb_prev->setText("<");
    _pb_prev->setEnabled(false);
    _pb_pause->setText("=");
    _pb_pause->setEnabled(false);
    _pb_next->setText(">");
    _pb_next->setEnabled(false);
    
    _volume_slider->setValue(0);
    _volume_slider->setMinimum(0);
    _volume_slider->setMaximum(100);
}

QString MyControl::getStr_notSelect()
{
    switch (_default_lan) 
    {
    default:
    case ENGLISH:
        return "No Video Playing...";
    case CHINESE:
        return "未选择文件";
    }
}

QString MyControl::getStr_playing()
{
    switch (_default_lan) 
    {
    default:
    case ENGLISH:
        return "Playing ";
    case CHINESE:
        return "正在播放 ";
    }
}

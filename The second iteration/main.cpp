/**
 * @file main.cpp
 * @brief The entrence of the app.
 * @version 2.0
 * @date 2022-12-04
 */
#include "mycontrol.h"
#include "myplayer.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    MyPlayer mainWindow;
    mainWindow.show();
    
    return app.exec();
}

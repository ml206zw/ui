/**
 * @file myplayer.cpp
 * @brief The source file of class MyPlayer, 
 *        including the definition of member 
 *        functions
 * @version 2.0
 * @date 2022-12-04
 */
#include "mymenu.h"
#include "myplayer.h"

#include <algorithm>

#include <QDebug>
#include <QString>
#include <QStringList>
#include <QPair>
#include <QVBoxLayout>
#include <QMediaPlayer>
#include <QFileDialog>
#include <QFileInfo>
#include <QDir>
#include <QDirIterator>
#include <QTextEdit>
#include <QMessageBox>
#include <QListWidget>
#include <QMediaPlaylist>
#include <QtMultimediaWidgets/QVideoWidget>

using namespace std;

MyPlayer::MyPlayer(QWidget *parent)
    : QMainWindow(parent),
      _default_lan(ENGLISH),
      _menu(new MyMenu(this, _default_lan)),
      _fileInfo(),
      _player(new QMediaPlayer(this)),
      _play_list(new QListWidget(this)),
      _control_bar(new MyControl(this)),
      _video_files()
{
    // init the window
    this->init();
    
    /*
     * ----------------------
     * |               |    |
     * |               |    |
     * |     player    |    |
     * |               |list|
     * -----------------    |
     * |  control bar  |    |
     * ----------------------
     */
    QWidget *central_widget = new QWidget(this);
    QHBoxLayout* main_layout = new QHBoxLayout(this);
    QVBoxLayout* left_layout = new QVBoxLayout(this);
    central_widget->setLayout(main_layout);
    // media player
    QVideoWidget* video_widget = new QVideoWidget;
    video_widget->setFixedSize(this->width() - SIZE_FIXED_LIST_WIDTH,
                               this->height() - SIZE_FIXED_CONTROL_HEIGHT);
    _player->setVolume(0);
    _player->setVideoOutput(video_widget);
    _player->setMedia(QMediaContent());
    // media list
    _play_list->setFixedSize(SIZE_FIXED_LIST_WIDTH, this->height());
    // add widgets to layout
    left_layout->addWidget(video_widget);
    left_layout->addWidget(_control_bar);
    main_layout->addLayout(left_layout);
    main_layout->addWidget(_play_list);
    
    this->setCentralWidget(central_widget);
    
    this->setMenuBar(this->_menu);
    
    connect(this->_menu->_map_file["open video"],   QAction::triggered,         this, MyPlayer::openVideo);
    connect(this->_menu->_map_file["open folder"],  QAction::triggered,         this, MyPlayer::openFolder);
    connect(this->_menu->_map_file["close all"],    QAction::triggered,         this, MyPlayer::closeVideo);
    connect(this->_menu->_map_language["English"],  QAction::triggered,         this, MyPlayer::setLanguage_en);
    connect(this->_menu->_map_language["Chinese"],  QAction::triggered,         this, MyPlayer::setLanguage_cn);
    connect(this->_player,                          SIGNAL(stateChanged(QMediaPlayer::State)), this, SLOT(playStateChanged(QMediaPlayer::State)));
    connect(this->_player,                          QMediaPlayer::positionChanged,   this, MyPlayer::judgeVideoTime);
    connect(this->_control_bar->_volume_slider,     QSlider::sliderMoved,       this, MyPlayer::judgeVolume);
    connect(this->_control_bar->_pb_pause,          QPushButton::clicked,       this, MyPlayer::changePlayerStatus);
    connect(this->_control_bar->_pb_next,           QPushButton::clicked,       this, MyPlayer::nextVideo);
    connect(this->_control_bar->_pb_prev,           QPushButton::clicked,       this, MyPlayer::prevVideo);    
    connect(this->_play_list,                       QListWidget::itemClicked,   this, MyPlayer::videoClicked);
}

MyPlayer::~MyPlayer()
{
}

void MyPlayer::init()
{
    this->setMinimumSize(SIZE_MINUMUM_WINDOW);
    this->setMaximumSize(SIZE_MAXIMUM_WINDOW);
    
    this->setWindowTitle("MyPlayer");
}

inline QString MyPlayer::setText_getFile_title(LANGUAGE lan)
{
    switch (lan) 
    {
    default:
    case ENGLISH:
        return "Choose File";
    case CHINESE:
        return "选择文件";
    }
}

inline QString MyPlayer::setText_getFile_type(LANGUAGE lan)
{
    switch (lan) 
    {
    default:
    case ENGLISH:
        return "Video Files(" VIDEO_TYPES ")";
    case CHINESE:
        return "视频文件(" VIDEO_TYPES ")";
    }
}

inline QString MyPlayer::setText_getDir_title(LANGUAGE lan)
{
    switch (lan) 
    {
    default:
    case ENGLISH:
        return "Choose A Directory";
    case CHINESE:
        return "选择文件夹";
    }
}

void MyPlayer::setLan(LANGUAGE lan)
{
    this->_default_lan = lan;
    this->_menu->setLan(lan);
    this->_control_bar->_default_lan = lan;
    updateControlLan();
}

void MyPlayer::updateControlLan()
{
    if (_player->state() == QMediaPlayer::State::PlayingState)
    {
        this->_control_bar->
                _title->setText(this->_control_bar->getStr_playing() +
                                _fileInfo.fileName());
    }
    else
    {
        this->_control_bar->
                _title->setText(this->_control_bar->getStr_notSelect());
    }
}

void MyPlayer::openVideo()
{
    DEBUG_CALL_FUNC();
    
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    setText_getFile_title(_default_lan),
                                                    DEFAULT_FILE_DIRECTORY,
                                                    setText_getFile_type(_default_lan));
    qDebug() << "Choose file name: " << fileName << endl;
    
    _fileInfo = QFileInfo(fileName);
    this->_player->setMedia(QUrl::fromLocalFile(fileName));
    if (_player->isAvailable()) 
    {
        _player->play(); 
        this->_control_bar->
                _time_slider->setMaximum(_player->duration());
        updateControlLan();
        this->_control_bar->
                _pb_pause->setEnabled(true);
    }
}

void MyPlayer::openFolder()
{
    DEBUG_CALL_FUNC();
    
    QFileDialog fileDialog;
    QString strTargetFile = fileDialog.getExistingDirectory(this,
                                                            setText_getDir_title(_default_lan),
                                                            DEFAULT_FILE_DIRECTORY);
    
    qDebug() << "Choose directory name: " << strTargetFile;
    QDir directory(strTargetFile);
    QDirIterator directory_iter(directory);
    
    while (directory_iter.hasNext())
    {
        QString fileName = directory_iter.next();
        if (fileName.contains(".wmv"))
        {
            QUrl file_url = QUrl(QUrl::fromLocalFile(fileName));
            this->_video_files.insert(file_url.fileName(), file_url);
        }
    }
    
    if (_video_files.size()) { this->_play_list->clear(); }
    for(auto info : _video_files)
    {
        qDebug() << "Read file: " << info.fileName();
        this->_play_list->addItem(info.fileName());
    }
    
    qDebug() << "key-values in map: ";
    for(auto iter = _video_files.begin(); iter != _video_files.end(); iter++)
    {
        qDebug() << iter.key() << "\t" << iter.value();
    }
}

void MyPlayer::closeVideo()
{
    DEBUG_CALL_FUNC();
    this->_player->setMedia(QMediaContent());
    this->_player->stop();
    updateControlLan();
    
    this->_play_list->clear();
}

void MyPlayer::setLanguage_en()
{
    DEBUG_CALL_FUNC();
    this->setLan(ENGLISH);
    this->_control_bar->_default_lan = ENGLISH;
}

void MyPlayer::setLanguage_cn()
{
    DEBUG_CALL_FUNC();
    this->setLan(CHINESE);
    this->_control_bar->_default_lan = CHINESE;
}

void MyPlayer::playStateChanged (QMediaPlayer::State ms) 
{
    DEBUG_CALL_FUNC();
    switch (ms) 
    {
    case QMediaPlayer::State::StoppedState:
//        if (this->_play_list->count())
//        {
//            static size_t index = 0;
//            index = _play_list->currentRow();
//            if (index >= _play_list->count()) { index = 0; }
//            QListWidgetItem* item = this->_play_list->item(++index);
//            videoClicked(item);
//            _play_list->setCurrentItem(item);
//            this->_control_bar->_title->setText(
//                        this->_control_bar->getStr_playing() + item->text()
//                        );
//        }
//        else
//        {
        updateControlLan();
//        }
        break;
    default:
        break;
    }
}

void MyPlayer::judgeVideoTime(qint64 position)
{
    if (this->_control_bar->_time_slider->maximum() != this->_player->duration())
    {
        this->_control_bar->_time_slider->setMaximum(this->_player->duration());
    }
    this->_control_bar->_time_slider->setValue(position);
    
    int seconds = (position / 1000) % 60;
    int minutes = (position / 60000) % 60;
    int hours = (position / 3600000) % 24;
    QTime time(hours, minutes, seconds);
    this->_control_bar->_media_time->setText(time.toString());
}

void MyPlayer::judgeVolume()
{
    int volume = this->_control_bar->_volume_slider->value();
    this->_player->setVolume(volume);
}

void MyPlayer::changePlayerStatus()
{
    if (this->_player->state() == QMediaPlayer::State::PausedState)
    {
        this->_player->play();
        this->_control_bar->_pb_pause->setText("=");
    }
    else if (this->_player->state() == QMediaPlayer::State::PlayingState)
    {
        this->_player->pause();
        this->_control_bar->_pb_pause->setText("<－biubiu－⊂(`ω´∩)");
    }
    updateControlLan();
}

void MyPlayer::nextVideo()
{
    if (_video_files.count())
    {
        int cur_index = this->_play_list->currentIndex().row();
        cur_index += 1;
        cur_index %= this->_play_list->count();
        QListWidgetItem *next_item = this->_play_list->item(cur_index);
        
        this->_play_list->setCurrentItem(next_item);
        videoClicked(next_item);
    }
    updateControlLan();
}

void MyPlayer::prevVideo()
{
    if (_video_files.count())
    {
        int cur_index = this->_play_list->currentIndex().row();
        cur_index -= 1;
        if (cur_index >= 0) { cur_index %= this->_play_list->count(); }
        else { cur_index =  this->_play_list->count() - 1; }
        QListWidgetItem *next_item = this->_play_list->item(cur_index);
        
        this->_play_list->setCurrentItem(next_item);
        videoClicked(next_item);
    }
    updateControlLan();
}

void MyPlayer::videoClicked(QListWidgetItem *item)
{
    this->_control_bar->_pb_pause->setEnabled(true);
    this->_control_bar->_pb_next->setEnabled(true);
    this->_control_bar->_pb_prev->setEnabled(true);
    QString title = item->text();
    qDebug() << "Clicked item: " << title;
    qDebug() << _video_files[title].fileName() << endl;
    
    this->_fileInfo = QFileInfo(_video_files[title].path());
    this->_player->setMedia(_video_files[title]);
    this->_player->play();
    
    updateControlLan();
}

/**
 * @file global.h
 * @brief The global configuration of project.
 * @version 2.0
 * @date 2022-12-04
 */
#ifndef GLOBAL_H
#define GLOBAL_H

/**
 * @brief The language of project.
 * 
 */
enum LANGUAGE
{
    ENGLISH,
    CHINESE
};

/**
 * @brief Debug macro function. Used to print log when a slot 
 * function is triggered.
 * 
 */
#define DEBUG_CALL_FUNC()   qDebug() << "Call " << __func__;

// #define VIDEO_TYPES "*.mp4 *.avi *.ps *.ts *.asf *.flv *.dahv *.mov"
#define VIDEO_TYPES "*.wmv"

// #define DEFAULT_FILE_DIRECTORY  "C:"
#define DEFAULT_FILE_DIRECTORY  "E:\\Desktop\\Not_finished\\2022-11-27 tec qt\\2811_cw2-master-release\\videos"

#endif // GLOBAL_H

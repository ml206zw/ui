/**
 * @file myplayer.h
 * @brief The head file of main window
 * @version 2.0
 * @date 2022-12-04
 */
#ifndef MYPLAYER_H
#define MYPLAYER_H

#include "global.h"
#include "mymenu.h"
#include "mycontrol.h"

#include <QMainWindow>
#include <QFileDialog>
#include <QMediaPlayer>
#include <QMediaMetaData>
#include <QListWidget>
#include <QTime>
#include <QWidget>
#include <QSize>
#include <QMap>

constexpr QSize SIZE_MINUMUM_WINDOW = QSize(800, 680);
constexpr QSize SIZE_MAXIMUM_WINDOW = QSize(1920, 1080);
constexpr int   SIZE_FIXED_LIST_WIDTH = 400;
constexpr int   SIZE_FIXED_CONTROL_HEIGHT = 200;

class MyPlayer : public QMainWindow
{
    Q_OBJECT
    
public:
    MyPlayer(QWidget *parent = nullptr);
    ~MyPlayer();
    
    /**
     * @brief Init the window, including :
     * 
     * 1. set the minimum & maximum size of window
     * 2. set the title of window
     */
    void init();
    
private:    
    LANGUAGE                    _default_lan;
    MyMenu*                     _menu;
    QFileInfo                   _fileInfo;
    QMediaPlayer*               _player;
    QListWidget*                _play_list;
    MyControl*                  _control_bar;
    typedef QString video_name;
    typedef QUrl    video;
    QMap<video_name, video>  _video_files; 
    
    inline QString setText_getFile_title(LANGUAGE lan);
    
    inline QString setText_getFile_type(LANGUAGE lan);
    
    inline QString setText_getDir_title(LANGUAGE lan);
    
    void setLan(LANGUAGE lan);
    
    void updateControlLan();
    
private slots:
    void openVideo();
    
    void openFolder();
    
    void closeVideo();
    
    void setLanguage_en();
    
    void setLanguage_cn();
    
    void playStateChanged (QMediaPlayer::State ms);
    
    void judgeVideoTime(qint64 position);
    
    void judgeVolume();
    
    void changePlayerStatus();
    
    void nextVideo();
    
    void prevVideo();
    
    void videoClicked(QListWidgetItem *item);
};
#endif // MYPLAYER_H
